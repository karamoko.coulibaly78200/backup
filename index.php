<?php 

require 'vendor/autoload.php';

$exclude = ['information_schema', 'mysql', 'performance_schema'];
$db_username = 'root';
$db_password = '';
$db = new PDO('mysql:host=localhost', $db_username, $db_password);
$db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
$q = $db->query('SHOW DATABASES');
$databases = $q->fetchAll();

$date = date('YmdHi');
foreach ($databases as $database) {
    $db_name = $database->Database;
    if (!in_array($db_name, $exclude)) {
        $file = $db_name . '.sql';
        try {
            $dump = new \Ifsnop\Mysqldump\Mysqldump('mysql:host=localhost;dbname='.$db_name, $db_username, $db_password);
            $dump->start('Databases/'.$date.$file);
            echo 'Toutes vos bases de données ont bien été sauvegardées';
        } catch (Exception $e) {
            echo 'Impossible de faire une sauvegarde : ' . $e->getMessage();
        }
    }
}